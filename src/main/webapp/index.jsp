<%-- 
    Document   : index
    Created on : 28-06-2021, 10:34:38
    Author     : Jano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Endpoints Api Equipos</h1>
        <h1>Listar Equipos - GET - https://evaluacion3al.herokuapp.com/api/equipos</h1>
        <h1>Listar Equipos por ID- GET - https://evaluacion3al.herokuapp.com/api/equipos{id}</h1>
        <h1>Agregar Equipos - POST - https://evaluacion3al.herokuapp.com/api/equipos</h1>
        <h1>Actualizar Equipos - PUT - https://evaluacion3al.herokuapp.com/api/equipos</h1>
        <h1>Eliminar Equipos - DELETE - https://evaluacion3al.herokuapp.com/api/equipos/{id}</h1>
    </body>
</html>
