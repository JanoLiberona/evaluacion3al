/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion3al.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evaluacion3al.dao.EquiposJpaController;
import root.evaluacion3al.dao.exceptions.NonexistentEntityException;
import root.evaluacion3al.entity.Equipos;

/**
 *
 * @author Jano
 */
    @Path("equipos")
public class equiposRest {
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarEquipos(){
        
        EquiposJpaController dao = new EquiposJpaController();
        List<Equipos> equipos = dao.findEquiposEntities();
        
        return Response.ok(200).entity(equipos).build();
        
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearEquipos(Equipos equipo){
        EquiposJpaController dao = new EquiposJpaController();
        try {
            dao.create(equipo);
        } catch (Exception ex) {
            Logger.getLogger(equiposRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(equipo).build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarEquipos(Equipos equipo){
        EquiposJpaController dao = new EquiposJpaController();
        try {
            dao.edit(equipo);
        } catch (Exception ex) {
            Logger.getLogger(equiposRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(equipo).build();
    }
    
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarEquipos(@PathParam("ideliminar")String eliminar){
        EquiposJpaController dao = new EquiposJpaController();
        try {
            dao.destroy(eliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(equiposRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("cliente eliminado").build();
    }
    
    @GET
    @Path("/{idConsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarEquipos(@PathParam("idConsultar")String idConsultar){
        
        EquiposJpaController dao = new EquiposJpaController();
        Equipos equipos = dao.findEquipos(idConsultar);
        
        return Response.ok(200).entity(equipos).build();
        
    }
    
    
}
